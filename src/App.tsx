import React from "react";
import SignIn from './pages/SignIn';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import SignUp from './pages/SignUp'

const App: React.FC<any> = () => {


  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path="/signin" component={SignIn} />
          <Route exact path="/signup" component={SignUp} />


        </Switch>
      </Router>
    </div>
  )

}

export default App;

